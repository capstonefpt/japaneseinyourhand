﻿using System;
using System.Collections.Generic;
using Microsoft.Office.Interop;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace convertDataToExcel
{
        class Program
        {
            private static string sourceInput, sourceOutput;
            private static int recordEachSheet = 20000;
            static void Main(string[] args)
            {
                Console.WriteLine("Program to load data from file to excel file");
                Console.WriteLine("Reading source file ");
                readingPath();
                Console.WriteLine("Reading text from file source input");
                List<string> alltext = readFile(sourceInput);

                Console.WriteLine("total data in file: " + alltext.Count + " records");
                Console.WriteLine();
                Console.WriteLine("input data to excel file");
                Console.WriteLine("processing.....");
                Console.WriteLine();
                inputDataToExcel(alltext);
                Console.WriteLine("\nDone");
                Console.ReadKey();
            }

        private static void readingPath()
        {
            try
            {


                System.IO.FileStream filestream = new System.IO.FileStream("config.txt",
                                              System.IO.FileMode.Open,
                                              System.IO.FileAccess.Read,
                                              System.IO.FileShare.ReadWrite);
                System.IO.StreamReader file = new System.IO.StreamReader(filestream, System.Text.Encoding.UTF8, true, 128);
                int countIndex = 0;
                string lineOfText;
                while ((lineOfText = file.ReadLine()) != null)
                {
                    if (countIndex == 0)
                    {
                        sourceInput = lineOfText;
                        countIndex++;
                    }
                    else
                    {
                        sourceOutput = lineOfText;
                        break;
                    }
                }
                if (sourceInput == null || sourceOutput == null || sourceOutput.Equals("") || sourceInput.Equals(""))
                {
                    Console.WriteLine("need input source file to file config.txt");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            } catch {
                Console.WriteLine("missing file config.txt");
                Console.ReadKey();
                Environment.Exit(0);
            }
        }

            private static void drawTextProgressBar(int progress, int total)
            {
                //draw empty progress bar
                Console.CursorLeft = 0;
                Console.Write("["); //start
                Console.CursorLeft = 32;
                Console.Write("]"); //end
                Console.CursorLeft = 1;
                float onechunk = 30.0f / total;

                //draw filled part
                int position = 1;
                for (int i = 0; i < onechunk * progress; i++)
                {
                    Console.BackgroundColor = ConsoleColor.Gray;
                    Console.CursorLeft = position++;
                    Console.Write(" ");
                }

                //draw unfilled part
                for (int i = position; i <= 31; i++)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.CursorLeft = position++;
                    Console.Write(" ");
                }

                //draw totals
                Console.CursorLeft = 35;
                Console.BackgroundColor = ConsoleColor.Black;
                Console.Write(progress.ToString() + " of " + total.ToString() + "    "); //blanks at the end remove any excess
            }

            private static void addTextToSheet(List<string> currentJob, Worksheet xlWorkSheet)
            {
                Console.WriteLine("convert data....");
                int currentrows = 1;
                int currentcols = 1;
                xlWorkSheet.Cells[currentrows, 1] = "kanji";
                xlWorkSheet.Cells[currentrows, 2] = "yomikata";
                xlWorkSheet.Cells[currentrows, 3] = "taipu";
                for (int i = 0; i < 30; i++)
                {
                    xlWorkSheet.Cells[currentrows, (i + 4)] = "imi " + (i + 1);
                }
                int index = 0;
                foreach (string currentLine in currentJob)
                {

                    currentrows++;
                    List<string> spilitLines = spilitLine(currentLine);
                    currentcols = 1;
                    foreach (string element in spilitLines)
                    {
                        xlWorkSheet.Cells[currentrows, currentcols] = element;
                        currentcols++;
                    }
                    index++;
                    drawTextProgressBar(index, currentJob.Count);



                }

                Console.WriteLine();
                Console.WriteLine("complete insert data into sheet " + xlWorkSheet.Name + " ....\n");
                Marshal.ReleaseComObject(xlWorkSheet);
            }

            private static List<string> readFile(string filename)
            {
                List<string> allText = new List<string>();
                System.IO.FileStream filestream = new System.IO.FileStream(filename,
                                              System.IO.FileMode.Open,
                                              System.IO.FileAccess.Read,
                                              System.IO.FileShare.ReadWrite);
                System.IO.StreamReader file = new System.IO.StreamReader(filestream, System.Text.Encoding.UTF8, true, 128);
                string lineOfText;
                while ((lineOfText = file.ReadLine()) != null)
                {
                    allText.Add(lineOfText);
                }
                file.Close();
                return allText;
            }
            private static void inputDataToExcel(List<string> allText)
            {
                Application xlApp = new Microsoft.Office.Interop.Excel.Application();

                if (xlApp == null)
                {
                    Console.WriteLine("Excel is not properly installed!!");
                    return;
                }


                Workbook xlWorkBook;
                Worksheet xlWorkSheet = null;
                object misValue = System.Reflection.Missing.Value;
                xlWorkBook = xlApp.Workbooks.Add(misValue);

                int totalSheet = (allText.Count / recordEachSheet);
                Console.WriteLine();
                Console.WriteLine("This file will be include " + totalSheet + " sheets");
                Console.WriteLine();
                for (int i = 0; i < totalSheet; i++)
                {
                    xlWorkBook.Sheets.Add();
                }
                int currentSheet = 0;
                List<string> currentJob = null;
                int index = 0;
                foreach (string currentLine in allText)
                {
                    if (index % recordEachSheet == 0 || index == allText.Count - 1)
                    {
                        if (index != 0)
                        {
                            Console.WriteLine();
                            currentSheet++;
                            Console.WriteLine("reference to sheet [" + currentSheet + "]....");
                            
                            xlWorkSheet = (Worksheet)xlWorkBook.Worksheets.get_Item(currentSheet);
                            xlWorkSheet.Name = "kanji sheet " + currentSheet;

                            addTextToSheet(currentJob, xlWorkSheet);


                        }
                        currentJob = new List<string>();
                        currentJob.Add(currentLine);
                        Console.WriteLine();


                    }
                    else
                    {
                        currentJob.Add(currentLine);
                    }
                    index++;
                }



                //xlWorkBook.Sheets.Move(After: xlWorkBook.Sheets.Count);
                // Console.WriteLine("Enter a name file and path to save file: ");
                //string path = Console.ReadLine();
                xlWorkBook.SaveAs(sourceOutput, XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);

                Console.WriteLine("\nExcel file data created");
            }

            private static List<string> spilitLine(string line)
            {
                line = line.Trim();
                List<string> result = new List<string>();
                string word = line.Substring(0, line.IndexOf(" "));
                line = line.Substring(line.IndexOf(" ")).Trim();
                result.Add(word);

                Match matchWord = Regex.Match(line, @"\[+[\p{L}]+\]", RegexOptions.IgnoreCase);
                if (matchWord.Success)
                {
                    string key = matchWord.Groups[0].Value;
                    line = line.Substring(line.IndexOf(key) + key.Length);
                    key = key.Substring(1, key.Length - 2);
                    result.Add(key);
                }
                else
                {
                    result.Add("");
                }

                Match matchType = Regex.Match(line, @"\(+[a-zA-Z0-9~`!@#%&-_+={}]+\)", RegexOptions.IgnoreCase);
                if (matchType.Success)
                {
                    string key = matchType.Groups[0].Value;
                    line = line.Substring(line.IndexOf(key) + key.Length).Trim();
                    key = key.Substring(1, key.Length - 2);
                    result.Add(key);
                }
                else
                {
                    result.Add("");
                }

                Match matchMean = Regex.Match(line, @"\(+[\d]+\)", RegexOptions.IgnoreCase);
                int countMatch = 0;
                while (matchMean.Success)
                {
                    countMatch++;
                    matchMean = matchMean.NextMatch();
                }
                if (countMatch > 0)
                {
                    for (int i = 1; i <= countMatch; i++)
                    {
                        try
                        {
                            try
                            {
                                line = line.Substring(line.IndexOf("(" + i + ")") + ("(" + i + ")").Length);
                            }
                            catch
                            {

                            }
                            if (i < countMatch)
                            {

                                string key = line.Substring(0, line.IndexOf("(" + (i + 1) + ")"));
                                result.Add(key);

                            }
                            else
                            {
                                result.Add(line);
                            }

                        }
                        catch
                        {

                        }

                    }
                }
                else
                {
                    result.Add(line);
                }

                return result;

            }
        
    }
}
