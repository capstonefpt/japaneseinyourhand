

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class Lesson
    {

        public Lesson()
        {
            this.LessonGrammars = new List<LessonGrammar>();
            this.LessonKanjis = new List<LessonKanji>();
            this.LessonQuizs = new List<LessonQuiz>();
            this.LessonWords = new List<LessonWord>();
            this.UserLessons = new List<UserLesson>();
        }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        [ForeignKey("Course")]
        public int CourseID { get; set; }

        [MaxLength(2000)]
        public string Description { get; set; }

        [MaxLength(500)]
        public string Image { get; set; }

        public string Dialogue { get; set; }

        public string DialogueAudio { get; set; }

        public string CultureNotes { get; set; }

        public bool Available { get; set; }
    
        public virtual Course Course { get; set; }


        public virtual List<LessonGrammar> LessonGrammars { get; set; }

        public virtual List<LessonKanji> LessonKanjis { get; set; }

        public virtual List<LessonQuiz> LessonQuizs { get; set; }

        public virtual List<LessonWord> LessonWords { get; set; }

        public virtual List<UserLesson> UserLessons { get; set; }
    }
}
