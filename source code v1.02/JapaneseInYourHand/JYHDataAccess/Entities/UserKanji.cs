

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class UserKanji
    {
        [Key, Column(Order = 0)]
        [ForeignKey("User")]
        public int UserID { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Kanji")]
        public int KanjiID { get; set; }

        public DateTime LearnedDate { get; set; }
    
        public virtual Kanji Kanji { get; set; }

        public virtual User User { get; set; }
    }
}
