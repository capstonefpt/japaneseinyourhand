

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class Word
    {
        public Word()
        {
            this.LessonWords = new List<LessonWord>();
            this.UserWords = new List<UserWord>();
        }
        
        [Key]
        public int ID { get; set; }

        [MaxLength(100)]
        public string Word1 { get; set; }

        [MaxLength(500)]
        public string Reading { get; set; }

        [MaxLength(500)]
        public string WReading1 { get; set; }

        [MaxLength(500)]
        public string WReading2 { get; set; }

        [MaxLength(2000)]
        public string Meaning { get; set; }

        [MaxLength(2000)]
        public string WMeaning1 { get; set; }

        [MaxLength(2000)]
        public string WMeaning2 { get; set; }

        [MaxLength(100)]
        public string Type { get; set; }

        public bool Common { get; set; }

        public bool UsuallyKana { get; set; }

        public bool SpecialReading { get; set; }

        public bool Available { get; set; }
    
        public virtual List<LessonWord> LessonWords { get; set; }

        public virtual List<UserWord> UserWords { get; set; }
    }
}
