﻿using JYHWebUI.Handlers;
using System;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace JYHWebUI.Controllers
{
    public class HomeController : NormalController
    {
        UserHandler userHandler = new UserHandler();
        public ActionResult Index()
        {
            var s = ViewBag.username;
            return View();
            
        }
        [HttpGet]
        public ActionResult Tips()
        {
            return View("Tips");

        }

        public JsonResult AutoReplyFeedback(string name,string email,string message)
        {
            try
            {
                 var fromAddress = new MailAddress("japaneseinyourhandwebsite@gmail.com");
                var toAddress = new MailAddress(email);
                MailMessage messages = new MailMessage(fromAddress, toAddress);
                const string fromPassword = "jlsfgktxltykddkx";//ipwssfjnnwfvpqhn
                messages.Subject = "[Japanese In Your Hand]-[Phản hồi góp ý!]";
                messages.Body = "" +
                    "   <p>Lời đầu tiên thay mặt hệ thống Japanese in Your Hand xin được chân thành cảm ơn những ý kiến đóng góp của bạn để giúp phần hoàn thiện website hơn nữa.</p>" +
                    "   <p>Sau đây là những ý kiến mà bạn đã gửi về cho chúng tôi:</p>" +
                    "         <h4>-Họ và tên     : " + name + ".</h4></p>" +
                    "         <h4>-Địa chỉ Email : " + email + ".</h4></p>" +
                    "         <h4>-Góp ý của bạn : " + message + ".</h4></p>" +
                    "   <p>Các Quản trị viên của hệ thống sẽ sớm liên lạc và có sự trợ giúp cũng như trả lời những ý kiến đóng góp của bạn một cách sớm nhất.</p>" +
                    "   <p>Mọi ý kiến đóng góp cũng như yêu cầu hỗ trợ xin vui lòng gửi về địa chỉ Email: japaneseinyourhandwebsite@gmail.com </p>" +
                    "   <p>Chúng tôi sẽ cố gắng giải đáp những thắc mắc trong vòng 2 ngày.</p>" +
                    "   <p>Cảm ơn bạn đã dành thời gian của bạn cho chúng tôi!</p>" +
                    "         </br><b style='font-size:24px;'>Website Japanese in Your Hand - Đường đến xử sở Mặt trời mọc</b>";


                messages.IsBodyHtml = true;
                var toAddress2 = new MailAddress("japaneseinyourhandwebsite@gmail.com");
                MailMessage messages2 = new MailMessage(fromAddress, toAddress2);
                messages2.Subject = "[Feedback]-[" + DateTime.UtcNow.Date + "] - [" + DateTime.UtcNow.Hour + "]";
                messages2.Body = "" +
                    "   <p>Feedback của người dùng.</p>" +
                    "   <p>Nội dung:</p>" +
                    "         <h3>-Họ và tên người dùng     : " + name + ".</h3>" +
                    "         <h3>-Địa chỉ Email người dùng : " + email + ".</h3>" +
                    "         <h3>-Góp ý của người dùng     : " + message + ".</h3>" +
                    "   <p><font style='color:red;'>Các Quản trị viên chú ý cố gắng giải quyết và trả lời câu hỏi của người dùng và trả lời thông qua Email trên.</font></p>" +
                    "   <p><font style='color:red;'>Thời gian hồi đáp cố gắng trong vòng 2 ngày nếu có vấn đề phát sinh phải báo lại cho người dùng.</font></p>" +
                    "   <p>Những Email Feedback đã được giải quyết bỏ đánh dấu quan trọng!</p>" +
                    "   <h3 style='color:red;'>Tuyệt đối không được xóa bỏ các Email Feedback này!</h3>" +
                    "         </br><b style='font-size:22px;'>Hãy cùng nhau xây dựng cộng đồng học Tiếng Nhật vững mạnh!</b>";


                messages2.IsBodyHtml = true;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword),
                    Timeout = 10000
                };

                {
                    smtp.Send(messages);
                    smtp.Send(messages2);
                    return Json("Email send", JsonRequestBehavior.AllowGet);
                }

                
            }catch(Exception e)
            {
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }

}