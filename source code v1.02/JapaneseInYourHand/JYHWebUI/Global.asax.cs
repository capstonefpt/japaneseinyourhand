﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using JYHDataAccess.DbService;

namespace JYHWebUI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            try
            {
                JYHDbContext jYHDbContext = new JYHDbContext();
                jYHDbContext.Database.Initialize(true);
                jYHDbContext.Database.CreateIfNotExists();
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
