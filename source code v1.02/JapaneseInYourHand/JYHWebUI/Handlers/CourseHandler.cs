﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHDataAccess.DbService;
using JYHDataAccess.Entities;
using JYHWebUI.Models.DTOs;
using System.Data.Entity;

namespace JYHWebUI.Handlers
{
    public class CourseHandler
    {
        JYHDbContext _jyhDbContext;

        public CourseHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public CourseHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public bool CheckExist(string name)
        {
            try
            {
                if (_jyhDbContext.Courses.Any(c => c.Name.Equals(name)))
                {
                    return true;
                }
            }
            catch { }
            return false;

        }

        public bool CheckExist(int id)
        {
            try
            {
                if (_jyhDbContext.Courses.Any(c => c.ID == id))
                {
                    return true;
                }
            }catch { }
            return false;
        }


        public List<CourseDto> Get()
        {
            try
            {
                var list =  _jyhDbContext.Courses.Where(c => c.Available == true).ToList().Select(
                    c => new CourseDto
                    {
                        ID = c.ID,
                        Name = c.Name,
                        Description = c.Description,
                        Level = c.Level,
                        Lessons = new LessonHandler().Get(c.ID).ToList()
                    }).ToList();
                if(list != null)
                {
                    return list;
                }
            }
            catch
            {
                
            }
            return new List<CourseDto>();
        }


        public List<CourseDto> Get(int userID)
        {
            try
            {
                var list = (
                from couses in _jyhDbContext.Courses.Where(c => c.Available == true) 
                join userCourse in _jyhDbContext.UserCourses.Where(uc => uc.UserID == userID) 
                on couses.ID  equals userCourse.CourseID into gj
                from x in gj.DefaultIfEmpty()
                select new CourseDto
                {
                    ID = couses.ID,
                    Name = couses.Name,
                    Description = couses.Description,
                    Level = couses.Level,
                    Progress = (x == null ? 0 : x.Progress)
                }).OrderByDescending(c => c.Progress).ToList();

                if(list != null)
                {
                    return list;
                }
              
                

            }
            catch
            {
                
            }
            return new List<CourseDto>();
        }

        public CourseDto CourseDetail(int course)
        {
            try
            {

                CourseDto courses = _jyhDbContext.Courses.Where(t => t.ID == course && t.Available == true).Select(c => new CourseDto
                {
                    ID = c.ID,
                    Name = c.Name,
                    Description = c.Description,
                    Level = c.Level,
                    Lessons = new LessonHandler().Get(c.ID).ToList()
                }).FirstOrDefault();
                if(courses != null)
                {
                    return courses;
                }
            }
            catch
            {
            }
            return new CourseDto();
        }

        public CourseDto GetCourseByLessonID(int lessonID)
        {
            try
            {
               var course =  _jyhDbContext.Lessons.Where(l => l.ID == lessonID).Join(_jyhDbContext.Courses.Where(c => c.Available == true)
                    , l => l.CourseID, c => c.ID, (l, c) => new CourseDto
                    {
                        ID = c.ID,
                        Description = c.Description,
                        Image = c.Image,
                        Level = c.Level,
                        Name = c.Name,

                    }).FirstOrDefault();
                if(course != null)
                {
                    return course;
                }
            }catch
            {
               
            }
            return new CourseDto();
        }

        public CourseDto CourseDetail(int course, int userID)
        {
            try
            {

                Course c = _jyhDbContext.Courses.FirstOrDefault(t => t.ID == course && t.Available == true);
                if (c != null)
                {
                
                    CourseDto a = new CourseDto()
                    {
                        ID = c.ID,
                        Name = c.Name,
                        Description = c.Description,
                        Level = c.Level,
                        Lessons = new LessonHandler().Get(c.ID, userID).ToList(),

                    };
                    return a;
                }


            }
            catch(Exception e)
            {
                
            }
            return new CourseDto();
        }

        public bool Add(CourseDto courseDto)
        {
            try
            {
                if (CheckExist(courseDto.Name))
                {
                    return false;
                }
                Course course = new Course
                {
                    Name = courseDto.Name,
                    Level = courseDto.Level,
                    Description = courseDto.Description,
                    Available = true

                };
                _jyhDbContext.Courses.Add(course);
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public bool Update(CourseDto courseDto)
        {
            try
            {
                if (!CheckExist(courseDto.ID))
                {
                    return false;
                }
                Course course = _jyhDbContext.Courses.FirstOrDefault(c => c.ID == courseDto.ID);
                course.Name = courseDto.Name;
                course.Level = courseDto.Level;
                course.Description = courseDto.Description;
                _jyhDbContext.Entry(course).State = EntityState.Modified;
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                if (!CheckExist(id))
                {
                    return false;
                }
                Course course = _jyhDbContext.Courses.FirstOrDefault(c => c.ID == id);
                course.Available = false;
                _jyhDbContext.Entry(course).State = EntityState.Modified;
                _jyhDbContext.SaveChanges();
                return true;
            }catch
            {
                return false;
            }
        }
    }
}