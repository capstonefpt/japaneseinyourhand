﻿
namespace JYHWebUI.Handlers
{
    using JYHDataAccess.DbService;
    using JYHDataAccess.Entities;
    using JYHWebUI.Models.DTOs;
    using System.Data.Entity;
    using System.Linq;
    using System.Collections.Generic;
    using Excel = Microsoft.Office.Interop.Excel;
    using System.Data;
    using System.Runtime.InteropServices;
    using System.Web.UI.WebControls;
    using JYHWebUI.Models.Dic;
    using System.Web;
    using OfficeOpenXml;
    using System;
    using System.Web.Mvc;
    using JYHDataAccess.Utilities;

    public class KanjiHandler
    {
        private JYHDbContext _jyhDbContext;

        public KanjiHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public KanjiHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public KanjiDto ConvertKanjiToDto(Kanji kanji)
        {
            KanjiDto kanjiDto = new KanjiDto();
            kanjiDto.ID = kanji.ID;
            kanjiDto.Kanji = kanji.Kanji1;
            kanjiDto.Radical = kanji.Radical;
            kanjiDto.Onyomi = kanji.Onyomi;
            kanjiDto.Kunyomi = kanji.Kunyomi;
            kanjiDto.Sino = kanji.Sino;
            kanjiDto.Meaning = kanji.Meaning;
            kanjiDto.StrokeCount = kanji.StrokeCount;
            kanjiDto.Mnemonic = kanji.Mnemonic;
            kanjiDto.Grade = kanji.Grade;
            kanjiDto.Level = kanji.Level;
            kanjiDto.Notes = kanji.Notes;

            return kanjiDto;
        }


        private bool CheckExist(int id)
        {
            Kanji Kanji = _jyhDbContext.Kanjis.FirstOrDefault(k => k.ID == id);
            if (Kanji != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                Kanji Kanji = _jyhDbContext.Kanjis.FirstOrDefault(k => k.ID == id);
                Kanji.Available = false;
                _jyhDbContext.Entry(Kanji).State = EntityState.Modified;
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }


        public List<KanjiDto> DicSearch(string text)
        {
            try
            {
                var list = _jyhDbContext.Kanjis.ToList().Where(k => text.Contains(k.Kanji1)
                || Utility.RemoveDiacritics(k.Sino.ToLower()).Equals(Utility.RemoveDiacritics(text.Trim().ToLower()))).Select(k => new KanjiDto
                {
                    ID = k.ID,
                    Kanji = k.Kanji1,
                    Sino = k.Sino,
                    Meaning = k.Meaning,
                    Onyomi = k.Onyomi,
                    Kunyomi = k.Kunyomi,
                    Notes = k.Notes,
                    Grade = k.Grade,
                    Level = k.Level,
                    Mnemonic = k.Mnemonic,
                    Radical = k.Radical,
                    StrokeCount = k.StrokeCount,
                    WKunyomi1 = k.WKunyomi1,
                    WKunyomi2 = k.WKunyomi2,
                    WOnyomi1 = k.WOnyomi1,
                    WOnyomi2 = k.WOnyomi2
                }).Take(5).ToList();
                if (list != null)
                {
                    return list;
                }
                
            }catch
            {

            }
            return new List<KanjiDto>();
        }

        public List<KanjiDto> Get(string prefix)
        {try
            {
                var list = _jyhDbContext.Kanjis.Where(k => k.Kanji1.StartsWith(prefix) ||
                k.Kunyomi.StartsWith(prefix) || k.Onyomi.StartsWith(prefix)
                ).Select(k => new KanjiDto
                {
                    ID = k.ID,
                    Kanji = k.Kanji1,
                    Meaning = k.Meaning == null ? "" : k.Meaning,
                    Onyomi = k.Onyomi == null ? "" : k.Onyomi,
                    Kunyomi = k.Kunyomi == null ? "" : k.Kunyomi,
                }).Take(5).ToList();
                if(list != null)
                {
                    return list;
                }
            }catch
            {

            }
            return new List<KanjiDto>();
        }

        public KanjiDto DicSearch(int id)
        {
            try
            {


                var kanji = _jyhDbContext.Kanjis.ToList().Where(k => k.ID == id).Select(k => new KanjiDto
                {
                    ID = k.ID,
                    Kanji = k.Kanji1,
                    Sino = k.Sino,
                    Meaning = k.Meaning,
                    Onyomi = k.Onyomi,
                    Kunyomi = k.Kunyomi,
                    Notes = k.Notes,
                    Grade = k.Grade,
                    Level = k.Level,
                    Mnemonic = k.Mnemonic,
                    Radical = k.Radical,
                    StrokeCount = k.StrokeCount,

                    WKunyomi1 = k.WKunyomi1,
                    WKunyomi2 = k.WKunyomi2,
                    WOnyomi1 = k.WOnyomi1,
                    WOnyomi2 = k.WOnyomi2
                }).FirstOrDefault();
                if (kanji != null)
                {
                    return kanji;
                }
            }
            catch
            {

            }
            return new KanjiDto();
        }

        #region import export
        public string Import(HttpPostedFileBase fileUpload)
        {
            try
            {
                string fileName = fileUpload.FileName;
                string fileContentType = fileUpload.ContentType;
                byte[] fileBytes = new byte[fileUpload.ContentLength];

                var data = fileUpload.InputStream.Read(fileBytes, 0, Convert.ToInt32(fileUpload.ContentLength));
                List<Kanji> listInsert = new List<Kanji>();
                List<Kanji> listUpdate = new List<Kanji>();
                using (var package = new ExcelPackage(fileUpload.InputStream))
                {
                    var currentSheet = package.Workbook.Worksheets;
                    var workSheet = currentSheet.First();
                    var noOfCol = workSheet.Dimension.End.Column;
                    var noOfRow = workSheet.Dimension.End.Row;
                    for (int rowIterator = 2; rowIterator < noOfRow; rowIterator++)
                    {
                        if (workSheet.Cells[rowIterator, 1].Value == null)
                        {
                            workSheet.Cells[rowIterator, 1].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 2].Value == null)
                        {
                            workSheet.Cells[rowIterator, 2].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 3].Value == null)
                        {
                            workSheet.Cells[rowIterator, 3].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 4].Value == null)
                        {
                            workSheet.Cells[rowIterator, 4].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 5].Value == null)
                        {
                            workSheet.Cells[rowIterator, 5].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 6].Value == null)
                        {
                            workSheet.Cells[rowIterator, 6].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 7].Value == null)
                        {
                            workSheet.Cells[rowIterator, 7].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 8].Value == null)
                        {
                            workSheet.Cells[rowIterator, 8].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 9].Value == null)
                        {
                            workSheet.Cells[rowIterator, 9].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 10].Value == null)
                        {
                            workSheet.Cells[rowIterator, 10].Value = "";

                        }
                        if (workSheet.Cells[rowIterator, 11].Value == null)
                        {
                            workSheet.Cells[rowIterator, 11].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 12].Value == null)
                        {
                            workSheet.Cells[rowIterator, 12].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 13].Value == null)
                        {
                            workSheet.Cells[rowIterator, 13].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 14].Value == null)
                        {
                            workSheet.Cells[rowIterator, 14].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 15].Value == null)
                        {
                            workSheet.Cells[rowIterator, 16].Value = "";

                        }
                        if (workSheet.Cells[rowIterator, 16].Value == null)
                        {
                            workSheet.Cells[rowIterator, 16].Value = "";
                        }
                        if (workSheet.Cells[rowIterator, 17].Value == null)
                        {
                            workSheet.Cells[rowIterator, 17].Value = "0";
                        }
                        var Kanji = new Kanji();
                        Kanji.ID = int.Parse(workSheet.Cells[rowIterator, 1].Value.ToString());
                        Kanji.Kanji1 = workSheet.Cells[rowIterator, 2].Value.ToString();
                        Kanji.Radical = workSheet.Cells[rowIterator, 3].Value.ToString();
                        Kanji.Sino = workSheet.Cells[rowIterator, 4].Value.ToString();
                        Kanji.Meaning = workSheet.Cells[rowIterator, 5].Value.ToString();
                        Kanji.Onyomi = workSheet.Cells[rowIterator, 6].Value.ToString();
                        Kanji.WOnyomi1 = workSheet.Cells[rowIterator, 7].Value.ToString();
                        Kanji.WOnyomi2 = workSheet.Cells[rowIterator, 8].Value.ToString();
                        Kanji.Kunyomi = workSheet.Cells[rowIterator, 9].Value.ToString();
                        Kanji.WKunyomi1 = workSheet.Cells[rowIterator, 10].Value.ToString();
                        Kanji.WKunyomi2 = workSheet.Cells[rowIterator, 11].Value.ToString();
                        Kanji.StrokeCount = int.Parse(workSheet.Cells[rowIterator, 12].Value.ToString());
                        Kanji.Mnemonic = workSheet.Cells[rowIterator, 13].Value.ToString();
                        Kanji.Grade = int.Parse(workSheet.Cells[rowIterator, 14].Value.ToString());
                        Kanji.Level = byte.Parse(workSheet.Cells[rowIterator, 15].Value.ToString());
                        Kanji.Notes = workSheet.Cells[rowIterator, 16].Value.ToString();
                        Kanji.Available = workSheet.Cells[rowIterator, 17].Value.ToString() == "1" ? true : false;



                        if (CheckExist(Kanji.ID))
                        {
                            listUpdate.Add(Kanji);
                        }
                        else
                        {
                            Kanji.Available = true;
                            listInsert.Add(Kanji);
                        }
                    }
                    using (JYHDbContext db = new JYHDbContext())
                    {
                        db.Configuration.AutoDetectChangesEnabled = false;
                        db.BulkInsert(listInsert);
                        db.BulkUpdate(listUpdate);
                        db.SaveChanges();

                    }
                }

                return "Nhập dữ liệu Kanji thành công. Thêm mới: " + listInsert.Count + ". Cập nhật: " + listUpdate.Count + ". Nhấn phím Back trên trình duyệt để quay trở lại.";
            }
            catch (Exception ex)
            {
                return "Nhập dữ liệu Kanji thất bại. Hãy kiểm tra lại định dạng file Excel.";
            }
        }

        public FileContentResult Export()
        {

            var fileDownloadName = String.Format("ExportKanji.xlsx");
            const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";


            // Pass your ef data to method
            ExcelPackage package = GenerateExcelFile(_jyhDbContext.Kanjis.ToList());

            var fsr = new FileContentResult(package.GetAsByteArray(), contentType);
            fsr.FileDownloadName = fileDownloadName;

            return fsr;
        }

        private static ExcelPackage GenerateExcelFile(IEnumerable<Kanji> datasource)
        {

            ExcelPackage pck = new ExcelPackage();

            //Create the worksheet 
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Kanji");
            
            // Sets Headers
            ws.Cells[1, 1].Value = "ID";
            ws.Cells[1, 2].Value = "Kanji";
            ws.Cells[1, 3].Value = "Radical";
            ws.Cells[1, 4].Value = "Sino";
            ws.Cells[1, 5].Value = "Meaning";
            ws.Cells[1, 6].Value = "Onyomi";
            ws.Cells[1, 7].Value = "WOnyomi1";
            ws.Cells[1, 8].Value = "WOnyomi2";
            ws.Cells[1, 9].Value = "Kunyomi";
            ws.Cells[1, 10].Value = "WKunyomi1";
            ws.Cells[1, 11].Value = "WKunyomi2";
            ws.Cells[1, 12].Value = "StrokeCount";
            ws.Cells[1, 13].Value = "Mnemonic";
            ws.Cells[1, 14].Value = "Grade";
            ws.Cells[1, 15].Value = "Level";
            ws.Cells[1, 16].Value = "Notes";
            ws.Cells[1, 17].Value = "Available";

            // Inserts Data
            for (int i = 0; i < datasource.Count(); i++)
            {
                ws.Cells[i + 2, 1].Value = datasource.ElementAt(i).ID;
                ws.Cells[i + 2, 2].Value = datasource.ElementAt(i).Kanji1;
                ws.Cells[i + 2, 3].Value = datasource.ElementAt(i).Radical;
                ws.Cells[i + 2, 4].Value = datasource.ElementAt(i).Sino;
                ws.Cells[i + 2, 5].Value = datasource.ElementAt(i).Meaning;
                ws.Cells[i + 2, 6].Value = datasource.ElementAt(i).Onyomi;
                ws.Cells[i + 2, 7].Value = datasource.ElementAt(i).WOnyomi1;
                ws.Cells[i + 2, 8].Value = datasource.ElementAt(i).WOnyomi2;
                ws.Cells[i + 2, 9].Value = datasource.ElementAt(i).Kunyomi;
                ws.Cells[i + 2, 10].Value = datasource.ElementAt(i).WKunyomi1;
                ws.Cells[i + 2, 11].Value = datasource.ElementAt(i).WKunyomi2;
                ws.Cells[i + 2, 12].Value = datasource.ElementAt(i).StrokeCount;
                ws.Cells[i + 2, 13].Value = datasource.ElementAt(i).Mnemonic;
                ws.Cells[i + 2, 14].Value = datasource.ElementAt(i).Grade;
                ws.Cells[i + 2, 15].Value = datasource.ElementAt(i).Level;
                ws.Cells[i + 2, 16].Value = datasource.ElementAt(i).Notes;
                ws.Cells[i + 2, 17].Value = datasource.ElementAt(i).Available ? "1" : "0";
            }

            // Format Header of Table
            using (ExcelRange rng = ws.Cells["A1:Z1"])
            {
                rng.Style.Font.Bold = true;
            }
            return pck;
        }
        #endregion
        public List<KanjiDto> Export(int level)
        {
            try
            {
                if (level == 0)
                {
                
                    var list =  _jyhDbContext.Kanjis.ToList().Select(k => new KanjiDto
                    {
                        ID = k.ID,
                        Kanji = k.Kanji1,
                        Radical = k.Radical,
                        Onyomi = k.Onyomi,
                        Kunyomi = k.Kunyomi,
                        Sino = k.Sino,
                        Meaning = k.Meaning,
                        StrokeCount = k.StrokeCount,
                        Mnemonic = k.Mnemonic,
                        Grade = k.Grade,
                        Level = k.Level,
                        Notes = k.Notes,
                    }).ToList();
                
                    if(list != null)
                    {
                        return list;
                    }
                }
                else
                {
                    var list = _jyhDbContext.Kanjis.Where(k => k.Level == level).ToList().Select(k => new KanjiDto
                    {
                        ID = k.ID,
                        Kanji = k.Kanji1,
                        Radical = k.Radical,
                        Onyomi = k.Onyomi,
                        Kunyomi = k.Kunyomi,
                        Sino = k.Sino,
                        Meaning = k.Meaning,
                        StrokeCount = k.StrokeCount,
                        Mnemonic = k.Mnemonic,
                        Grade = k.Grade,
                        Level = k.Level,
                        Notes = k.Notes,
                    }).ToList();
                    if (list != null)
                    {
                        return list;
                    }
                }
            }
            catch
            {

            }
            return new List<KanjiDto>();
        }


    }
}