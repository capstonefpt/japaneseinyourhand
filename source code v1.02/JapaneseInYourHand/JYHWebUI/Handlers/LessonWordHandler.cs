﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHDataAccess.Entities;
using JYHDataAccess.DbService;
using JYHWebUI.Models.DTOs;
using JYHWebUI.Models.Dic;

namespace JYHWebUI.Handlers
{
    public class LessonWordHandler
    {
        JYHDbContext _jyhDbContext;
        public LessonWordHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public LessonWordHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        private bool CheckExist(LessonWordDto lessonWordDto)
        {
            try
            {
                LessonWord lessonWord = _jyhDbContext.LessonWords.FirstOrDefault(lw =>
                lw.LessonID == lessonWordDto.LessonID && lw.WordID == lessonWordDto.WordID);
                if (lessonWord == null)
                {
                    return false;
                }
                return true;
            }catch 
            {
                return false;
            }
        }

        public bool Add(LessonWordDto lessonWordDto)
        {
            if(CheckExist(lessonWordDto))
            {
                return false;
            }

            LessonWord lesson = new LessonWord
            {
                LessonID = lessonWordDto.LessonID,
                WordID = lessonWordDto.WordID,
               // Available = true
                
            };
            try
            {
                _jyhDbContext.LessonWords.Add(lesson);
                _jyhDbContext.SaveChanges();
                return true;
            }catch
            {
                return false;
            }
        }

        public bool Delete(LessonWordDto lesswordDto)
        {
            if(!CheckExist(lesswordDto))
            {
                return false;
            }
            try
            {
                LessonWord lesson = _jyhDbContext.LessonWords.FirstOrDefault(
                lw => lw.LessonID == lesswordDto.LessonID && lw.WordID == lesswordDto.WordID);
                _jyhDbContext.LessonWords.Remove(lesson);
                _jyhDbContext.SaveChanges();
                return true;
            }catch 
            {
                return false;
            }
        }

        public List<WordDto> Get(int lessonID)
        {
            try
            {
                var list = _jyhDbContext.LessonWords.Where(lw => lw.LessonID == lessonID).Join(_jyhDbContext.Words,
                                                            lw => lw.WordID,
                                                            w => w.ID,
                                                            (lw, w) => new WordDto
                                                            {
                                                                ID = w.ID,
                                                                Word = w.Word1,
                                                                Reading = w.Reading,
                                                                Meaning = w.Meaning,
                                                                Type = w.Type,
                                                                Common = w.Common,
                                                                SpecialReading = w.SpecialReading,
                                                                UsuallyKana = w.UsuallyKana,
                                                                WMeaning1 = w.WMeaning1,
                                                                WMeaning2 = w.WMeaning2,
                                                                WReading1 = w.WReading1,
                                                                WReading2 = w.WReading2
                                                            }).ToList();

               if(list == null)
                {
                    return new List<WordDto>();
                }
                return list;
            }catch
            {
                return new List<WordDto>();
            }
        }

        private string ConvertTypeToString(int type)
        {
            switch (type)
            {
                case 2:
                    return "Danh từ";
                case 3:
                    return "Động từ nhóm I";
                case 4:
                    return "Động từ nhóm II";
                case 5:
                    return "Động từ nhóm III";
                case 6:
                    return "Động từ";
                case 7:
                    return "Tính từ đuôi い";
                case 8:
                    return "Tính từ đuôi な";
                case 9:
                    return "Phó từ";
                default:
                    return "";
            }

        }

        public List<LessonWordDto> GetID(int lessonID)
        {
            try
            {
                var list = _jyhDbContext.LessonWords.Where(lw => lw.LessonID == lessonID).Select(lw => new LessonWordDto
                {
                    WordID = lw.WordID
                }).ToList();
                if(list == null)
                {
                    return new List<LessonWordDto>();
                }
                return list;
            }
            catch
            {
                return new List<LessonWordDto>();
            }
        }

    }
}