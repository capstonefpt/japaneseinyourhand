﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class UserLessonDto
    {
        [DataMember(Name ="userID", EmitDefaultValue =false)]
        public int UserID { get; set; }

        [DataMember(Name = "lessonID", EmitDefaultValue = false)]
        public int LessonID { get; set; }

        [DataMember(Name = "score", EmitDefaultValue = false)]
        public int Score { get; set; }

        [DataMember(Name = "progress", EmitDefaultValue = false)]
        public int Progress { get; set; }

        [DataMember(Name = "startDate", EmitDefaultValue = false)]
        public DateTime StartDate { get; set; }

        [DataMember(Name = "completeDate", EmitDefaultValue = false)]
        public DateTime CompleteDate { get; set; }

        [DataMember(Name = "lastLearnedDate", EmitDefaultValue = false)]
        public DateTime LastLearnedDate { get; set; }
    }
}