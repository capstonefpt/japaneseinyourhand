﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class LessonDto
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int ID { get; set; }

        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember(Name = "score", EmitDefaultValue = false)]
        public int Score { get; set; }

        [DataMember(Name = "progress", EmitDefaultValue = false)]
        public int Progress { get; set; }

        [DataMember(Name = "courseID", EmitDefaultValue = false)]
        public int CourseID { get; set; }

        [DataMember(Name = "image", EmitDefaultValue = false)]
        public string Image { get; set; }

        [DataMember(Name = "description", EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(Name = "dialogue", EmitDefaultValue = false)]
        public string Dialogue { get; set; }

        [DataMember(Name = "dialogueAudio", EmitDefaultValue = false)]
        public string DialogueAudio { get; set; }

        [DataMember(Name = "cultureNotes", EmitDefaultValue = false)]
        public string CultureNotes { get; set; }

        [DataMember(Name = "lessonWords", EmitDefaultValue = false)]
        public List<WordDto> LessonWords { get; set; }

        [DataMember(Name = "lessonKanjis", EmitDefaultValue = false)]
        public List<KanjiDto> LessonKanjis { get; set; }

        [DataMember(Name = "lessonGrammars", EmitDefaultValue = false)]
        public List<GrammarDto> LessonGrammars { get; set; }

        [DataMember(Name = "lessonQuizs", EmitDefaultValue = false)]
        public List<LessonQuizDto> LessonQuizs { get; set; }

        [DataMember(Name = "userGrammars", EmitDefaultValue = false)]
        public List<UserGrammarDto> UserGrammars { get; set; }

        [DataMember(Name = "userKanjis", EmitDefaultValue = false)]
        public List<UserKanjiDto> UserKanjis { get; set; }

        [DataMember(Name = "userWords", EmitDefaultValue = false)]
        public List<UserWordDto> UserWords { get; set; }
    }
}