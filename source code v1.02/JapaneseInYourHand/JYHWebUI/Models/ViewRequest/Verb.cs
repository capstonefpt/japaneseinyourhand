﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHWebUI.Models.DTOs;

namespace JYHWebUI.Models.ViewRequest
{
    public class Verb
    {
        public string Answer { get; set; }
        public string  Result { get; set; }
        public WordDto WordDto { get; set; }
    }
}