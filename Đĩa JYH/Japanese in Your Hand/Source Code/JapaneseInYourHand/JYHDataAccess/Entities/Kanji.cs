

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class Kanji
    {
        public Kanji()
        {
            this.LessonKanjis = new List<LessonKanji>();
            this.UserKanjis = new List<UserKanji>();
        }
    
        [Key]
        public int ID { get; set; }

        [MaxLength(1)]
        public string Kanji1 { get; set; }

        [MaxLength(1)]
        public string Radical { get; set; }

        [MaxLength(500)]
        public string Onyomi { get; set; }

        [MaxLength(500)]
        public string WOnyomi1 { get; set; }

        [MaxLength(500)]
        public string WOnyomi2 { get; set; }

        [MaxLength(500)]
        public string Kunyomi { get; set; }

        [MaxLength(500)]
        public string WKunyomi1 { get; set; }

        [MaxLength(500)]
        public string WKunyomi2 { get; set; }

        [MaxLength(50)]
        public string Sino { get; set; }

        [MaxLength(2000)]
        public string Meaning { get; set; }

        public int StrokeCount { get; set; }

        [MaxLength(2000)]
        public string Mnemonic { get; set; }

        public int Grade { get; set; }

        public Nullable<byte> Level { get; set; }

        public string Notes { get; set; }

        public bool Available { get; set; }
    
       
        public virtual List<LessonKanji> LessonKanjis { get; set; }
      
        public virtual List<UserKanji> UserKanjis { get; set; }
    }
}
