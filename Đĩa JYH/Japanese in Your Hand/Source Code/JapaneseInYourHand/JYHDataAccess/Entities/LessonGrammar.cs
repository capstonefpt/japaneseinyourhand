

namespace JYHDataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;

    public partial class LessonGrammar
    {
        [Key, Column(Order = 0)]
        [ForeignKey("Lesson")]
        public int LessonID { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Grammar")]
        public int GrammarID { get; set; }
    
        public virtual Grammar Grammar { get; set; }

        public virtual Lesson Lesson { get; set; }
    }
}
