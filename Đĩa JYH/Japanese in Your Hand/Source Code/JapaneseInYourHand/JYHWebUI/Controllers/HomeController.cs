﻿using JYHWebUI.Handlers;
using JYHWebUI.Models.ViewRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JYHWebUI.Models.DTOs;

namespace JYHWebUI.Controllers
{
    public class HomeController : Controller
    {
        UserHandler userHandler = new UserHandler();

        
        // GET: Home
        public ActionResult Index()
        {
                return View();
            
        }
        [HttpPost]
        public int Available()
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            //kiểm tra đã đăng nhập chưa
            if (session == null)
            {
                //chưa đăng nhập thì kiểm tra xem có lưu cookie đăng nhập không
                HttpCookie ACookie = Request.Cookies[Common.Common.account];
                HttpCookie ICookie = Request.Cookies[Common.Common.identifier];
                HttpCookie TCookie = Request.Cookies[Common.Common.token];
                //xem có cookie chưa
                if (ACookie != null && ICookie != null && TCookie != null)
                {
                    //có cookie rồi
                    CookieDto cookie = new CookieDto() { Account = ACookie.Value, IDEC = ICookie.Value, token = TCookie.Value };
                    //check cookie đúng không
                    if (userHandler.CheckSession(cookie))
                    {
                        //nếu đúng sinh ra Session["Available"]
                        Session[Common.Common.user] = userHandler.Get(cookie.Account);
                        return 1;
                    }
                    else
                    {
                        //nếu sai thông báo phiên đăng nhập hết hạn gọi vào Logout để xóa hết cookie và session đi yêu cầu đăng nhập lại
                        return 0;
                    }
                }
                else
                {
                    //chưa có cookie
                    return 0;
                }
                
            }
            else
            {
                //nếu có đăng nhập rồi
                if (userHandler.Available(session.Identify))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }             
            }
        } 
        public ActionResult UnAccess()
        {
            return PartialView();
        }
        public ActionResult Access()
        {
            ViewBag.Role = 1;
            return PartialView();
        }
        [HttpGet]
        public JsonResult Users()
        {
            UserDto session = HttpContext.Session[Common.Common.user] as UserDto;
            return Json(session, JsonRequestBehavior.AllowGet);
        }
    }


}