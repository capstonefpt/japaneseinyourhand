﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JYHDataAccess.DbService;
using JYHDataAccess.Entities;
using JYHWebUI.Models.DTOs;
using System.Data.Entity;

namespace JYHWebUI.Handlers
{
    public class UserStatHandler
    {
        JYHDbContext _jyhDbContext;

        public UserStatHandler()
        {
            _jyhDbContext = new JYHDbContext();
        }

        public UserStatHandler(JYHDbContext jyhDbContext)
        {
            _jyhDbContext = jyhDbContext;
        }

        public UserStatDto Get(int userID)
        {
            try
            {
                var user = _jyhDbContext.UserStats.Where(ut => ut.UserID == userID).Select(ut => new UserStatDto
                {
                    UserID = ut.UserID,
                    CorrectGrammar = ut.CorrectGrammar,
                    CorrectKanji = ut.CorrectKanji,
                    CorrectListening = ut.CorrectListening,
                    CorrectReading = ut.CorrectReading,
                    CorrectWord = ut.CorrectWord,
                    TotalTime = ut.TotalTime,
                    WrongGrammar = ut.WrongGrammar,
                    WrongKanji = ut.WrongKanji,
                    WrongListening = ut.WrongListening,
                    WrongReading = ut.WrongReading,
                    WrongWord = ut.WrongWord
                }).FirstOrDefault();
                return user;
            }
            catch
            {
                return null;
            }
        }

        public bool CheckExist(int userID)
        {
            try
            {
                if (_jyhDbContext.UserStats.Where(us => us.UserID == userID).Any())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        public bool Add(int userID)
        {
            if (CheckExist(userID))
            {
                return false;
            }
            UserStat user = new UserStat
            {
                WrongWord = 0,
                CorrectGrammar = 0,
                CorrectKanji = 0,
                CorrectListening = 0,
                CorrectReading = 0,
                CorrectWord = 0,
                TotalTime = 0,
                UserID = userID,
                WrongGrammar = 0,
                WrongKanji = 0,
                WrongListening = 0,
                WrongReading = 0
            };

            try
            {
                _jyhDbContext.UserStats.Add(user);
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Update(UserStatDto userStatDto)
        {
            try
            {
                var user = _jyhDbContext.UserStats.Where(us => us.UserID == userStatDto.UserID).FirstOrDefault();
                if (user != null)
                {
                    user.WrongWord = userStatDto.WrongWord;
                    user.CorrectWord = userStatDto.CorrectWord;
                    user.WrongGrammar = userStatDto.WrongGrammar;
                    user.CorrectGrammar = userStatDto.CorrectGrammar;
                    user.WrongKanji = userStatDto.WrongKanji;
                    user.CorrectKanji = userStatDto.CorrectKanji;
                    user.WrongListening = userStatDto.WrongListening;
                    user.CorrectListening = userStatDto.CorrectListening;
                    user.WrongReading = userStatDto.WrongReading;
                    user.CorrectReading = userStatDto.CorrectReading;
                    user.TotalTime = userStatDto.TotalTime;
                }
                _jyhDbContext.Entry(user).State = EntityState.Modified;
                _jyhDbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }
        public void UpdateTime(int userID)
        {
            try
            {
                var user = _jyhDbContext.UserStats.FirstOrDefault(us => us.UserID == userID);
                if (user != null)
                {
                    user.TotalTime += 1;
                }
                _jyhDbContext.Entry(user).State = EntityState.Modified;
                _jyhDbContext.SaveChanges();
            }
            catch
            {
            }
        }
    }
}