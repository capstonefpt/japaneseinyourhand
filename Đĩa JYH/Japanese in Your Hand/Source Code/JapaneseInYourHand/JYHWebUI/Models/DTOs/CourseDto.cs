﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class CourseDto
    {
        [DataMember(Name ="id", EmitDefaultValue = false)]
        public int ID { get; set; }

        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }

        [DataMember(Name = "level", EmitDefaultValue = false)]
        public byte? Level { get; set; }

        [DataMember(Name = "image", EmitDefaultValue = false)]
        public string Image { get; set; }

        [DataMember(Name = "description", EmitDefaultValue = false)]
        public string Description { get; set; }

        [DataMember(Name = "lessons", EmitDefaultValue = false)]
        public List<LessonDto> Lessons { get; set; }

        [DataMember(Name = "progress", EmitDefaultValue = false)]
        public int Progress { get; set; }
    }
}