﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class LessonGrammarDto
    {
        [DataMember(Name = "lessonID", EmitDefaultValue = false)]
        public int LessonID { get; set; }

        [DataMember(Name = "grammarID", EmitDefaultValue = false)]
        public int GrammarID { get; set; }
    }
}