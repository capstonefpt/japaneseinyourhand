﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    public class LessonQuizDto
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int ID { get; set; }

        [DataMember(Name = "lessonID", EmitDefaultValue = false)]
        public int LessonID { get; set; }

        [DataMember(Name = "question", EmitDefaultValue = false)]
        public string Question { get; set; }

        [DataMember(Name = "correctAnswer", EmitDefaultValue = false)]
        public string CorrectAnswer { get; set; }

        [DataMember(Name = "wrongAnswer1", EmitDefaultValue = false)]
        public string WrongAnswer1 { get; set; }

        [DataMember(Name = "wrongAnswer2", EmitDefaultValue = false)]
        public string WrongAnswer2 { get; set; }

        [DataMember(Name = "wrongAnswer3", EmitDefaultValue = false)]
        public string WrongAnswer3 { get; set; }

        [DataMember(Name = "type", EmitDefaultValue = false)]
        public byte? Type { get; set; }

        [DataMember(Name = "explanation", EmitDefaultValue = false)]
        public string Explanation { get; set; }
    }
}