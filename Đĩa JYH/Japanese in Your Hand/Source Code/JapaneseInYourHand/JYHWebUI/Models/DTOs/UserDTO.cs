﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace JYHWebUI.Models.DTOs
{
    [DataContract]
    public class UserDto
    {
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public int ID { get; set; }

        [DataMember(Name = "email", EmitDefaultValue = false)]
        public string Email { get; set; }

        [DataMember(Name = "password", EmitDefaultValue = false)]
        public string Password { get; set; }

        [DataMember(Name = "fullname", EmitDefaultValue = false)]
        public string FullName { get; set; }

        [DataMember(Name = "phone", EmitDefaultValue = false)]
        public string Phone { get; set; }

        [DataMember(Name = "address", EmitDefaultValue = false)]
        public string Address { get; set; }

        [DataMember(Name = "level", EmitDefaultValue = false)]
        public byte? Level { get; set; }

        [DataMember(Name = "identify", EmitDefaultValue = false)]
        public string Identify { get; set; }

        [DataMember(Name = "token", EmitDefaultValue = false)]
        public string Token { get; set; }

        [DataMember(Name = "createdate", EmitDefaultValue = false)]
        public DateTime CreatedDate { get; set; }

        [DataMember(Name = "lastlogin", EmitDefaultValue = false)]
        public DateTime LastLogin { get; set; }

    }
}