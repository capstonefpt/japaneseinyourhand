﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace JYHWebUI.Models.ViewRequest
{
    public class CookieDto
    {
        [DataMember(Name = "Account", EmitDefaultValue = false)]
        public string Account { get; set; }
        [DataMember(Name = "IDEC", EmitDefaultValue = false)]
        public string IDEC { get; set; }
        [DataMember(Name = "token", EmitDefaultValue = false)]
        public string token { get; set; }
    }
}