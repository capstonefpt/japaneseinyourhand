﻿using System;
using JYHWebUI.Handlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JYHWebUI
{
    [TestClass]
    public class ConvertFullNameTest
    {
        // --------------------------------------------------------------------------------
        // ConvertNameCheckProcess001 ~ ConvertNameCheckProcess018
        // 
        // These test methods check whether input is correctly trimmed,
        // punctuation marks are correctly removed, diacritics are correctly processed,
        // and whether spaces are correctly converted to ・ characters.
        // これらのテストメソッドは、入力値が正しくトリムされるか、
        // 約物が正しく削除されるか、ダイアクリティックが正しく処理されるか、
        // スペースが正しく「・」になるか確認する。
        // --------------------------------------------------------------------------------
        [TestMethod]
        public void ConvertNameCheckProcess001()
        {
            var name = "Nhật Minh";
            var expected = "ニャット・ミン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess002()
        {
            var name = "Nguyễn Nhật Minh";
            var expected = "グエン・ニャット・ミン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess003()
        {
            var name = "Nguyễn Nhật Minh ";
            var expected = "グエン・ニャット・ミン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess004()
        {
            var name = " Nguyễn Nhật Minh";
            var expected = "グエン・ニャット・ミン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess005()
        {
            var name = "       Nguyễn Nhật Minh       ";
            var expected = "グエン・ニャット・ミン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess006()
        {
            var name = "Nguyễn,.;:()[]{}<>'\"?!- Nhật Minh";
            var expected = "グエン・ニャット・ミン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess007()
        {
            var name = "a á à ả ã ạ";
            var expected = "アー・アー・アー・アー・アー・アー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess008()
        {
            var name = "ă ắ ằ ẳ ẵ ặ";
            var expected = "アー・アー・アー・アー・アー・アー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess009()
        {
            var name = "â ấ ầ ẳ ẫ ậ";
            var expected = "アー・アー・アー・アー・アー・アー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess010()
        {
            var name = "e é è ẻ ẽ ẹ";
            var expected = "エー・エー・エー・エー・エー・エー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess011()
        {
            var name = "ê ế ề ể ễ ệ";
            var expected = "エー・エー・エー・エー・エー・エー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess012()
        {
            var name = "i í ì ỉ ĩ ị";
            var expected = "イー・イー・イー・イー・イー・イー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess013()
        {
            var name = "o ó ò ỏ õ ọ";
            var expected = "オー・オー・オー・オー・オー・オー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess014()
        {
            var name = "ô ố ồ ổ ỗ ộ";
            var expected = "オー・オー・オー・オー・オー・オー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess015()
        {
            var name = "ơ ớ ờ ở ỡ ợ";
            var expected = "オー・オー・オー・オー・オー・オー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess016()
        {
            var name = "u ú ù ủ ũ ụ";
            var expected = "ウー・ウー・ウー・ウー・ウー・ウー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess017()
        {
            var name = "ư ứ ừ ử ữ ự";
            var expected = "ウー・ウー・ウー・ウー・ウー・ウー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCheckProcess018()
        {
            var name = "y ý ỳ ỷ ỹ ỵ";
            var expected = "イー・イー・イー・イー・イー・イー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        // --------------------------------------------------------------------------------
        // ConvertNameAbnormal001 ~ ConvertNameAbnormal004
        // 
        // These test methods check whether abnormal inputs are correctly processed.
        // これらのテストメソッドは、異常な入力値が正しく処理されるか確認する。
        // --------------------------------------------------------------------------------
        [TestMethod]
        public void ConvertNameAbnormal001()
        {
            var name = "";
            var expected = "";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameAbnormal002()
        {
            var name = "Qu";
            var expected = "qu";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameAbnormal003()
        {
            var name = "Az";
            var expected = "アz";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameAbnormal004()
        {
            var name = "B";
            var expected = "bー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameAbnormal005()
        {
            var name = ".";
            var expected = "";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        // --------------------------------------------------------------------------------
        // ConvertNameCase001 ~ ConvertNameCase148
        // 
        // These test methods cover all the different cases for conversion of Vietnamese
        // word to Katakana, ensuring that all Vietnamese words convert as intended.
        // これらのテストメソッドは、すべての変換ケースをテストし、
        // ベトナムの単語がすべて正しくカタカナに変換することを確認する。
        // --------------------------------------------------------------------------------


        [TestMethod]
        public void ConvertNameCase001()
        {
            var name = "A";
            var expected = "アー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase002()
        {
            var name = "Ba";
            var expected = "バー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase003()
        {
            var name = "Be";
            var expected = "ベー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase004()
        {
            var name = "Bi";
            var expected = "ビー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase005()
        {
            var name = "Bo";
            var expected = "ボー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase006()
        {
            var name = "Bu";
            var expected = "ブー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase007()
        {
            var name = "Ca";
            var expected = "カー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase008()
        {
            var name = "Co";
            var expected = "コー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase009()
        {
            var name = "Cu";
            var expected = "クー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase010()
        {
            var name = "Cha";
            var expected = "チャー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase011()
        {
            var name = "Che";
            var expected = "チェー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase012()
        {
            var name = "Chi";
            var expected = "チー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase013()
        {
            var name = "Cho";
            var expected = "チョー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase014()
        {
            var name = "Chu";
            var expected = "チュー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase015()
        {
            var name = "Da";
            var expected = "ザー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase016()
        {
            var name = "De";
            var expected = "ゼー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase017()
        {
            var name = "Di";
            var expected = "ジー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase018()
        {
            var name = "Do";
            var expected = "ゾー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase019()
        {
            var name = "Du";
            var expected = "ズー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase020()
        {
            var name = "Đa";
            var expected = "ダー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase021()
        {
            var name = "Đe";
            var expected = "デー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase022()
        {
            var name = "Đi";
            var expected = "ディ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase023()
        {
            var name = "Đo";
            var expected = "ドー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase024()
        {
            var name = "Đu";
            var expected = "ドゥ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase025()
        {
            var name = "E";
            var expected = "エー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase026()
        {
            var name = "Ga";
            var expected = "ガー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase027()
        {
            var name = "Ghe";
            var expected = "ゲー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase028()
        {
            var name = "Ghi";
            var expected = "ギー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase029()
        {
            var name = "Gi";
            var expected = "ジー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase030()
        {
            var name = "Gia";
            var expected = "ジャー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase031()
        {
            var name = "Gie";
            var expected = "ジェー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase032()
        {
            var name = "Gio";
            var expected = "ジョー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase033()
        {
            var name = "Giu";
            var expected = "ジュー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase034()
        {
            var name = "Go";
            var expected = "ゴー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase035()
        {
            var name = "Gu";
            var expected = "グー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase036()
        {
            var name = "Ha";
            var expected = "ハー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase037()
        {
            var name = "He";
            var expected = "ヘー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase038()
        {
            var name = "Hi";
            var expected = "ヒー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase039()
        {
            var name = "Ho";
            var expected = "ホー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase040()
        {
            var name = "Hu";
            var expected = "フー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase041()
        {
            var name = "I";
            var expected = "イー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase042()
        {
            var name = "Ke";
            var expected = "ケー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase043()
        {
            var name = "Kha";
            var expected = "カー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase044()
        {
            var name = "Khe";
            var expected = "ケー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase045()
        {
            var name = "Khi";
            var expected = "キー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase046()
        {
            var name = "Kho";
            var expected = "コー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase047()
        {
            var name = "Khu";
            var expected = "クー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase048()
        {
            var name = "Ki";
            var expected = "キー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase049()
        {
            var name = "La";
            var expected = "ラー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase050()
        {
            var name = "Le";
            var expected = "レー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase051()
        {
            var name = "Li";
            var expected = "リー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase052()
        {
            var name = "Lo";
            var expected = "ロー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase053()
        {
            var name = "Lu";
            var expected = "ルー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase054()
        {
            var name = "Ma";
            var expected = "マー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase055()
        {
            var name = "Me";
            var expected = "メー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase056()
        {
            var name = "Mi";
            var expected = "ミー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase057()
        {
            var name = "Mo";
            var expected = "モー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase058()
        {
            var name = "Mu";
            var expected = "ムー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase059()
        {
            var name = "Na";
            var expected = "ナー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase060()
        {
            var name = "Ne";
            var expected = "ネー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase061()
        {
            var name = "Nga";
            var expected = "ガー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase062()
        {
            var name = "Nghe";
            var expected = "ゲー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase063()
        {
            var name = "Nghi";
            var expected = "ギー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase064()
        {
            var name = "Ngo";
            var expected = "ゴー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase065()
        {
            var name = "Ngu";
            var expected = "グー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase066()
        {
            var name = "Nha";
            var expected = "ニャー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase067()
        {
            var name = "Nhe";
            var expected = "ニェー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase068()
        {
            var name = "Nhi";
            var expected = "ニー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase069()
        {
            var name = "Nho";
            var expected = "ニョー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase070()
        {
            var name = "Nhu";
            var expected = "ヌー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase071()
        {
            var name = "Ni";
            var expected = "ニー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase072()
        {
            var name = "No";
            var expected = "ノー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase073()
        {
            var name = "Nu";
            var expected = "ヌー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase074()
        {
            var name = "O";
            var expected = "オー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase075()
        {
            var name = "Pa";
            var expected = "パー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase076()
        {
            var name = "Pe";
            var expected = "ペー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase077()
        {
            var name = "Pha";
            var expected = "ファー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase078()
        {
            var name = "Phe";
            var expected = "フェー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase079()
        {
            var name = "Phi";
            var expected = "フィ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase080()
        {
            var name = "Pho";
            var expected = "フォー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase081()
        {
            var name = "Phu";
            var expected = "フー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase082()
        {
            var name = "Pi";
            var expected = "ピー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase083()
        {
            var name = "Po";
            var expected = "ポー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase084()
        {
            var name = "Pu";
            var expected = "プー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase085()
        {
            var name = "Qua";
            var expected = "クア";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase086()
        {
            var name = "Que";
            var expected = "クエ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase087()
        {
            var name = "Qui";
            var expected = "クイ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase088()
        {
            var name = "Quo";
            var expected = "クオ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase089()
        {
            var name = "Ra";
            var expected = "ラー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase090()
        {
            var name = "Re";
            var expected = "レー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase091()
        {
            var name = "Ri";
            var expected = "リー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase092()
        {
            var name = "Ro";
            var expected = "ロー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase093()
        {
            var name = "Ru";
            var expected = "ルー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase094()
        {
            var name = "Sa";
            var expected = "サー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase095()
        {
            var name = "Se";
            var expected = "セー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase096()
        {
            var name = "Si";
            var expected = "シー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase097()
        {
            var name = "So";
            var expected = "ソー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase098()
        {
            var name = "Su";
            var expected = "スー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase099()
        {
            var name = "Ta";
            var expected = "ター";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase100()
        {
            var name = "Te";
            var expected = "テー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase101()
        {
            var name = "Tha";
            var expected = "ター";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase102()
        {
            var name = "The";
            var expected = "テー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase103()
        {
            var name = "Thi";
            var expected = "ティ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase104()
        {
            var name = "Tho";
            var expected = "トー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase105()
        {
            var name = "Thu";
            var expected = "トゥ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase106()
        {
            var name = "Ti";
            var expected = "ティ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase107()
        {
            var name = "To";
            var expected = "トー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase108()
        {
            var name = "Tra";
            var expected = "チャー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase109()
        {
            var name = "Tre";
            var expected = "チェー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase110()
        {
            var name = "Tri";
            var expected = "チー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase111()
        {
            var name = "Tro";
            var expected = "チョー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase112()
        {
            var name = "Tru";
            var expected = "チュー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase113()
        {
            var name = "Tu";
            var expected = "トゥ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase114()
        {
            var name = "U";
            var expected = "ウー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase115()
        {
            var name = "Va";
            var expected = "ヴァー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase116()
        {
            var name = "Ve";
            var expected = "ヴェー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase117()
        {
            var name = "Vi";
            var expected = "ヴィ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase118()
        {
            var name = "Vo";
            var expected = "ヴォー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase119()
        {
            var name = "Vu";
            var expected = "ヴー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase120()
        {
            var name = "Xa";
            var expected = "サー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase121()
        {
            var name = "Xe";
            var expected = "セー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase122()
        {
            var name = "Xi";
            var expected = "シー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase123()
        {
            var name = "Xo";
            var expected = "ソー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase124()
        {
            var name = "Xu";
            var expected = "スー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase125()
        {
            var name = "Ly";
            var expected = "リー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase126()
        {
            var name = "Hoa";
            var expected = "ホア";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase127()
        {
            var name = "Huê";
            var expected = "フエ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase128()
        {
            var name = "Mai";
            var expected = "マイ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase129()
        {
            var name = "Cao";
            var expected = "カオ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase130()
        {
            var name = "Lưu";
            var expected = "ルー";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase131()
        {
            var name = "Châu";
            var expected = "チョウ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase132()
        {
            var name = "Khuya";
            var expected = "クイア";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase133()
        {
            var name = "Đức";
            var expected = "ドゥック";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase134()
        {
            var name = "Bách";
            var expected = "バック";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase135()
        {
            var name = "Khuyên";
            var expected = "クエン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase136()
        {
            var name = "Khuyết";
            var expected = "クエット";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase137()
        {
            var name = "Hoài";
            var expected = "ホアイ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase138()
        {
            var name = "Lâm";
            var expected = "ラム";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase139()
        {
            var name = "Lan";
            var expected = "ラン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase140()
        {
            var name = "Long";
            var expected = "ロン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase141()
        {
            var name = "Linh";
            var expected = "リン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase142()
        {
            var name = "Nguyễn";
            var expected = "グエン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase143()
        {
            var name = "Ngoao";
            var expected = "ゴアオ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase144()
        {
            var name = "Lập";
            var expected = "ラップ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase145()
        {
            var name = "Tất";
            var expected = "タット";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase146()
        {
            var name = "Hươu";
            var expected = "フオウ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase147()
        {
            var name = "Xoay";
            var expected = "ソアイ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void ConvertNameCase148()
        {
            var name = "Quẩy";
            var expected = "クエイ";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ConvertNameCase149()
        {
            var name = "Anh";
            var expected = "アイン";
            var actual = new AlphabetHandler().ConvertFullName(name.Trim());
            Assert.AreEqual(expected, actual);
        }


    }
}
